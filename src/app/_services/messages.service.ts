import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  messages: Message[] = [];
  dialogue: Message[] = [];
  story: string[] = [];

  constructor() { }

  add(message: Message) {
    this.messages.push(message);
    this.autoClear();
    console.log(message.input);
    console.log(message.response);
  }

  autoClear() {
    if (this.messages.length > 5) {
      this.messages.shift();
    }
  }
}
export interface Message {
  input: string;
  response: string;
}
