import { Injectable } from '@angular/core';

import { PlayerConsoleComponent } from '../player-console/player-console.component';
import { Item } from '../_ts/item';
import { MessagesService } from './messages.service';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  console: PlayerConsoleComponent;
  log: MessagesService;

  constructor() { }

  moveItem(target: Item, origin: Item[], destination: Item[]) {
    this.log.add({
      input: `[take]`,
      response: `added ${target.name} to inventory`
    });
    destination.splice(target.id, 0, target);
    this.log.add({
      input: `[take]`,
      response: `removed ${target.name} from scene.`
    });
    origin.splice(target.id, 1);
  }
}
