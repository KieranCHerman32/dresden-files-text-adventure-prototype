import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LittleChicagoComponent } from './little-chicago.component';

describe('LittleChicagoComponent', () => {
  let component: LittleChicagoComponent;
  let fixture: ComponentFixture<LittleChicagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LittleChicagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LittleChicagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
