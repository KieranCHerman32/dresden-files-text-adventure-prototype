import { Component, OnInit } from '@angular/core';

import { Typewriter } from 't-writer.js';

import { Chapter } from '../_ts/chapter';

@Component({
  selector: 'app-log-book',
  templateUrl: './log-book.component.html',
  styleUrls: ['./log-book.component.css'],
})
export class LogBookComponent implements OnInit {
  chapter: Chapter;
  tWriter: Typewriter;

  constructor() { }

  ngOnInit(): void { }

}
