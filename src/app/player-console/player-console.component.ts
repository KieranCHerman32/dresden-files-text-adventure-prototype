import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ChapterTest } from '../_chapters/testChapter';
import { MessagesService } from '../_services/messages.service';
import { PlayerCharacter } from '../_ts/player';
import {
  CharacterCommands,
  CharacterModifiers,
  Commands, ItemCommands,
  ItemModifiers,
  Modifiers,
  MoveCommands,
  MoveModifiers,
  Articles
} from '../_ts/commands';
import { ItemsService } from '../_services/items.service';

@Component({
  selector: 'app-player-console',
  templateUrl: './player-console.component.html',
  styleUrls: ['./player-console.component.css'],
})
export class PlayerConsoleComponent implements OnInit {
  // items: Item[] = [
  //   {
  //     id: 0,
  //     name: 'hat',
  //     canTake: false,
  //     seen: false,
  //     status: 'inScene',
  //     startingLoc: {
  //       chapter: 0,
  //       id: 0,
  //       name: 'Intro'
  //     },
  //   },
  //   {
  //     id: 1,
  //     name: 'duster',
  //     canTake: true,
  //     seen: false,
  //     status: 'inScene',
  //     startingLoc: {
  //       chapter: 0,
  //       id: 0,
  //       name: 'Intro'
  //     },
  //   },
  // ];
  // inventory: Item[] = [];

  story = ChapterTest;
  player = PlayerCharacter;

  commands = Commands;
  modifiers = Modifiers;
  articles = Articles;

  itemCmds = ItemCommands;
  itemMods = ItemModifiers;

  charCmds = CharacterCommands;
  charMods = CharacterModifiers;

  moveCmds = MoveCommands;
  moveMods = MoveModifiers;

  // TargetTypes and ModifierType may be removed later
  // They provide no purpose outside of verifying against my logic.
  input: string;
  targetType: string;
  target: any;
  target2Type: string;
  target2: string;
  commandType: string;
  command: string;
  modifier: string;
  modifierType: string;

  userConsole = new FormGroup({
    userInput: new FormControl(''),
  });

  locations = this.story.locations;

  constructor(
    public messagesService: MessagesService,
    public itemControl: ItemsService
  ) { }

  ngOnInit(): void { }

  // Reads player input and parses logic
  readInput() {
    // Takes user input on submission, splits input into array, and removes articles from array.
    this.input = this.userConsole.value.userInput;
    const cmd = this.input.split(' ');
    this.removeArticles(cmd);
    const command = cmd[0];
    const cmdLength = cmd.length;
    console.log(this.input);
    console.log(cmd);
    // Gates logic based on the type of command being issued
    if (this.isCmd(command, this.itemCmds)) {
      this.parseCmd(cmd, cmdLength, 'item');
    } else if (this.isCmd(command, this.charCmds)) {
      this.parseCmd(cmd, cmdLength, 'character');
    } else if (this.isCmd(command, this.moveCmds)) {
      this.parseCmd(cmd, cmdLength, 'location');
    } else if (this.isCmd(command, this.commands)) {
      this.parseCmd(cmd, cmdLength, 'general');
    } else {
      this.messagesService.add({
        input: `[${this.input}]`,
        response: `I'm not sure what you want me to do.`
      });
    }
    this.resetUserInput();
  }

  // Resets input and related fields.
  resetUserInput() {
    this.input = '';
    this.commandType = '';
    this.command = '';
    this.targetType = '';
    this.target = '';
    this.target2Type = '';
    this.userConsole.reset();
  }

  // Removes Articles from player input | Removes extraneous data from input.
  removeArticles(input) {
    for (let i = 0; i < input.length; i++) {
      if (this.articles.includes(input[i])) {
        input.splice(i--, 1);
      }
    }
  }

  // User input consists of a command without a target.
  parseCmd(cmd: string[], cmdLength: number, commandType: string) {
    this.command = cmd[0];
    switch (commandType) {
      case 'item':
        switch (cmd.length) {
          case 1:
            this.messagesService.add({
              input: `[${this.input}]`,
              response: `I'm not sure what you want me to do. Try "${this.input}" and then the name of an Item.`
            });
            break;
          case 2:
            this.target = cmd[1];
            this.targetType = this.commandType;
            this.isValidTarget(this.targetType);
            // Run method to execute command based on Command and Target
            break;
          case 3:
            this.target = cmd[2];
            if (this.isMod(cmd[1], this.modifiers)) {
              this.modifier = cmd[1];
              this.isValidTarget(this.targetType);
              // Run method to execute command based on Command, Modifier, and Target
            }
            break;
          case 4:
            this.target = cmd[1];
            this.modifier = cmd[2];
            this.target2 = cmd[3];
        }
        break;
      case 'character':
        this.messagesService.add({
          input: `[${this.input}]`,
          response: `I'm not sure what you want me to do. Try "${this.input}" and then the name of a Character.`
        });
        break;
      case 'location':
        this.messagesService.add({
          input: `[${this.input}]`,
          response: `I'm not sure what you want me to do. Try "${this.input}" and then the name of a Location.`
        });
        break;
      case 'general':
        this.messagesService.add({
          input: `[${this.input}]`,
          response: `I'm not sure what you want me to do. Try "${this.input}" and then the name of an Item, Character, or Location.`
        });
        break;
      default:
        this.messagesService.add({
          input: `[${this.input}]`,
          response: `I'm not sure what you want me to do.`
        });
        break;
    }
  }

  // User input includes command and target. Command must be compared to target to verify target is valid for type of command detected.

  // Verify cmd[0] exists in given list of commands
  isCmd(input, list) {
    if (list.includes(input)) {
      switch (list) {
        case this.itemCmds:
          this.commandType = 'item';
          break;
        case this.charCmds:
          this.commandType = 'character';
          break;
        case this.moveCmds:
          this.commandType = 'location';
          break;
        case this.commands:
          this.commandType = 'general';
          break;
        default:
          this.commandType = null;
          break;
      }
      return true;
    } else {
      return false;
    }
  }

  isMod(input, list) {
    if (list.includes(input)) {
      switch (list) {
        case this.itemMods:
          this.modifierType = 'item';
          break;
        case this.charMods:
          this.modifierType = 'character';
          break;
        case this.moveMods:
          this.modifierType = 'location';
          break;
        case this.modifiers:
          this.modifierType = 'general';
          break;
        default:
          break;
      }
      return true;
    } else {
      return false;
    }
  }

  isValidMod(targetType) {
    switch (targetType) {
      case 'item':
        this.modifierType = 'item';
        break;
      case 'character':
        this.modifierType = 'character';
        break;
      case 'location':
        this.modifierType = 'location';
        break;
      case 'general':
        this.modifierType = 'general';
        break;
      default:
        break;
    }
  }

  // Verify cmd[i] is a valid target for cmd[0]
  isValidTarget(commandType) {
    switch (commandType) {
      case 'item':
        if (this.isLocalItem(this.target)) {
          // Run command on item in location
        } else if (this.isInvItem(this.target)) {
          // Run command on item in inventory
        }
        break;
      case 'character':
        if (this.isLocalCharacter(this.target)) {
          // Run command on Character in location
        } else if (this.isContact(this.target)) {
          // Run command on Character in Contacts
        }
        break;
      case 'location':
        if (this.isConnectedLoc(this.target)) {
          // Run command on connected location
        } else if (this.isKnownLoc(this.target)) {
          // Run command on known location
        }
        break;
      case 'general':
        // May be more complicated than other cases.
        // Put this on hold until functions are in place to execute commands based on player input.
        break;
      default:
        break;
    }
    console.log(this.target);
  }

  // Verifies that item with matching name exists in the player's location.
  isLocalItem(target) {
    const playerLoc = this.player.locale;
    const items = this.story.locations[playerLoc].items;
    // Loops through all items in current location
    for (const item of items) {
      // Verifies that name is matched, item.isSeen: true, item.canTake: true.
      if (
        item.name === target &&
        item.isSeen === true &&
        item.canTake === true) {
        this.target = item;
        return item;
      }
    }
  }

  // Verifies that item with matching name exists in player's inventory.
  isInvItem(target) {
    const inventory = this.player.inventory;
    for (const item of inventory) {
      if (item.name === target) {
        this.target = item;
        return item;
      }
    }
  }

  // Verifies that character with matching name exists in player's location.
  isLocalCharacter(target) {
    const playerLoc = this.player.locale;
    const characters = this.story.characters;
    // Loops through all Characters in Chapter Object
    for (const character of characters) {
      // Verifies that a Character has known: true and Character location matches player location.
      if (
        character.name === target &&
        character.known === true &&
        character.location === playerLoc) {
        this.target = character;
        return character;
      }
    }
  }

  // Verifies that character with matching name exists in the player's contacts
  isContact(target) {
    const contacts = this.player.contacts;
    const characters = this.story.characters;
    // Loops through all characters in the chapter
    for (const character of characters) {
      // Loops through all characters in player contacts.
      for (const contact of contacts) {
        // Compares character.id to contact.id, and character.name to target.
        if (
          character.name === target &&
          character.id === contact) {
          this.target = character;
          return character;
        }
      }
    }
  }

  // Verifies that location is connected to the player's location.
  isConnectedLoc(target) {
    const locations = this.story.locations;
    const playerLoc = this.player.locale;
    for (const location of locations) {
      if (
        location.name === target &&
        location.connectedLocs.includes(playerLoc)) {
        this.target = location;
        return location;
      }
    }
  }

  // Verifies that location is known by the player.
  isKnownLoc(target) {
    const locations = this.story.locations;
    // Loops through all locations in chapter
    for (const location of locations) {
      // Verifies that player knows location and that name is matched.
      if (
        location.isKnown === true &&
        location.name === target) {
        this.target = location;
        return location;
      }
    }
  }
}
