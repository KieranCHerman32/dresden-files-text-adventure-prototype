import { Locale } from '../_ts/Locale';
import { Chapter } from '../_ts/chapter';

export const ChapterTest: Chapter = {
  titleCard: 'Test',
  id: 0,
  text: [
    'You awake in a dark room.',
    'It is too dark to see very far in front of you.',
    'There is an unlit torch and a match sitting at your feet.'
  ],
  locations: [
    {
      id: 0,
      connectedLocs: [1, 2, 3, 4],
      name: 'start',
      items: [
        // TORCH
        {
          id: 0,
          partnerId: 1,
          name: 'torch',
          canTake: true,
          isSeen: true,
          seen: [
            'There is a TORCH at my feet.',
            'A TORCH. It\'s still wet with fuel.'
          ],
          examine: [
            'A TORCH, wet with fuel. I should find a way to light it and find my way out.',
            'If I can LIGHT it with the MATCH I found, I should be able to navigate safely.',
            'The TORCH burns brightly, your body welcomes the warmth.'
          ],
          status: 'unlit',
          container: {
            inContainer: false,
          },
          startingLoc: {
            chapter: 0,
            locId: 'startRoom',
            name: 'start',
          },
        },
        // MATCH
        {
          id: 1,
          partnerId: 0,
          name: 'match',
          canTake: true,
          isSeen: false,
          seen: [
            'A match lies discarded on the floor'
          ],
          examine: [
            'A single MATCH.',
            'If I\'m lucky, I can use it to LIGHT my TORCH.'
          ],
          status: 'unused',
          container: {
            inContainer: false
          },
          startingLoc: {
            chapter: 0,
            locId: 'startRoom',
            name: 'start'
          },
        },
        // RED DOOR
        {
          id: 2,
          partnerId: 0,
          name: 'red door',
          canTake: false,
          isSeen: false,
          seen: [
            'A red door',
            'It\'s locked.',
            'The red door slides into the wall, leaving an open doorway',
            'The red door slides back into place, blocking my path.'
          ],
          examine: [
            'A sturdy red door.',
            'It\'s locked tight.',
            'I feel like it should be painted black.',
            'I\'ve never seen a lock like this before.',
            'I can\'t tell where the wall ends and the door begins. Is this magic?'
          ],
          status: 'locked',
          container: {
            inContainer: false
          },
          startingLoc: {
            chapter: 0,
            locId: 'startRoom',
            name: 'start'
          }
        },
        {
          id: 3,
          partnerId: 0,
          name: 'blue door',
          canTake: false,
          isSeen: false,
          seen: [
            'A blue door',
            'It\'s locked.',
            'The red door slides into the wall, leaving an open doorway',
            'The red door slides back into place, blocking my path.'
          ],
          examine: [
            'A sturdy blue door.',
            'It\'s locked tight.',
            'I\'ve never seen a lock like this before.',
            'I can\'t tell where the wall ends and the door begins. Is this magic?'
          ],
          status: 'locked',
          container: {
            inContainer: false
          },
          startingLoc: {
            chapter: 0,
            locId: 'startRoom',
            name: 'start'
          }
        }
      ],
      description: [
        'The room is too dark to see anything else.',
        `The torchlight illuminates the room. I see a BLUE DOOR to the WEST, a RED DOOR to the EAST, and hallways to the NORTH and SOUTH`
      ],
      isKnown: false,
      visited: false
    },
  ],
  characters: []
};

