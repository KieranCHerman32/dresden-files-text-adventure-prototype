import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { PlayerConsoleComponent } from './player-console/player-console.component';
import { ContactsComponent } from './contacts/contacts.component';
import { InventoryComponent } from './inventory/inventory.component';
import { JournalComponent } from './journal/journal.component';
import { LogBookComponent } from './log-book/log-book.component';
import { LittleChicagoComponent } from './little-chicago/little-chicago.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayerConsoleComponent,
    ContactsComponent,
    InventoryComponent,
    JournalComponent,
    LogBookComponent,
    LittleChicagoComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatSidenavModule,
    MatExpansionModule,
    ReactiveFormsModule,
    MatTableModule,
    NgbModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
