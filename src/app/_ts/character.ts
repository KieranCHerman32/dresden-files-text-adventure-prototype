import { Item } from './item';

export interface Character {
  id: number;
  known: boolean;
  name: string;
  status: string;
  contact?: string;
  description: string;
  dialogue?: string[];
  inventory?: Item[];
  location: number;
}
