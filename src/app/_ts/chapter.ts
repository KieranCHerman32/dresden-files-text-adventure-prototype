import { Locale } from './Locale';
import { Character } from './character';

export interface Chapter {
  titleCard: string;
  id: number;
  text: string[];
  locations: Locale[];
  characters: Character[];
}

