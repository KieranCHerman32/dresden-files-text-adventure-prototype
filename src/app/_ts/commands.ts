export const Commands: string[] = [
  'take',
  'get',
  'drop',
  'put',
  'give',
  'examine',
  'x',
  'open',
  'close',
  'lock',
  'unlock',
  'show',
  'cover',
  'search',
  'talk',
  'ask',
  'tell',
  'say',
  'go',
  'move'
];
export const Modifiers: string[] = [

];

export const ItemCommands: string[] = [
  'take',
  'get',
  'drop',
  'put',
  'give',
  'examine',
  'x',
  'open',
  'close',
  'lock',
  'unlock',
  'show',
  'cover',
  'search'
];
export const ItemModifiers: string[] = [
  'above',
  'behind',
  'below',
  'beneath',
  'beside',
  'besides',
  'between',
];

export const CharacterCommands: string[] = [
  'talk',
  'ask',
  'tell',
  'say',
];
export const CharacterModifiers: string[] = [
  'about',
];

export const MoveCommands: string[] = [
  'go',
  'move'
];
export const MoveModifiers: string[] = [

];

export const Articles: string[] = [
  'a',
  'an',
  'some',
  'the'
];

export const Prepositions: string[] = [
  'aboard',
  'across',
  'after',
  'against',
  'along',
  'amid',
  'among',
  'anti',
  'around',
  'as',
  'at',
  'before',
  'beyond',
  'but',
  'by',
  'concerning',
  'considering',
  'despite',
  'down',
  'during',
  'except',
  'excepting',
  'excluding',
  'following',
  'for',
  'from',
  'in',
  'inside',
  'into',
  'like',
  'minus',
  'near',
  'of',
  'off',
  'on',
  'onto',
  'opposite',
  'outside',
  'over',
  'past',
  'per',
  'plus',
  'regarding',
  'round',
  'save',
  'since',
  'than',
  'through',
  'to',
  'toward',
  'towards',
  'under',
  'underneath',
  'unlike',
  'until',
  'up',
  'upon',
  'versus',
  'via',
  'with',
  'within',
  'without',
];
