import { Item } from './item';
import { Character } from './character';

export interface Locale {
  id: number;
  connectedLocs: number[];
  name: string;
  characters?: number[];
  items?: Item[];
  description: string[];
  isKnown: boolean;
  visited: boolean;
}
