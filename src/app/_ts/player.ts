import { Item } from './item';

export interface Player {
  chapter: number;
  textId: number;
  status: string;
  locale: number;
  canLeave: boolean;
  inventory: Item[];
  contacts: number[];
  journal: string[];
}

export const PlayerCharacter: Player = {
  chapter: 0,
  textId: 0,
  status: 'Alive. For now.',
  locale: 0,
  canLeave: true,
  inventory: [],
  contacts: [],
  journal: []
};
