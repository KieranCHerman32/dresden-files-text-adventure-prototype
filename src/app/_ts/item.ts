export interface Item {
  // item.id & item.partnerId should relate to another item with a direct relationship to the current item.
  id: number;
  partnerId?: number;
  name: string;
  canTake: boolean;
  isSeen: boolean;
  seen: string[];
  examine: string[];
  status: string;
  container: {
    inContainer: boolean;
    container?: string;
    containerStatus?: string;
  };
  startingLoc: {
    chapter: number;
    locId: string;
    name: string;
  };
}
